from django import forms
from forum.models import Subject, Post

class subjectForm(forms.ModelForm):

    class Meta:
        model = Subject
        fields = ['subjectTitle']

class postForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ['postText']