from django.db import models
from django.contrib.auth.models import User
from accounts.models import Profile
from tinymce import models as tinymce_models

# Create your models here.
class Category(models.Model):
    categoryName = models.CharField(max_length=50)
    categoryDescription = models.TextField(max_length=200, blank=True)

    def __str__(self):
        return self.categoryName

    def count_subjects(self):
        return self.subjects.count()

class Subject(models.Model):
    subjectTitle = models.CharField(max_length=200, blank=False)
    subjectDate = models.DateField(auto_now=True)
    subjectCategory = models.ForeignKey(Category, related_name='subjects')
    subjectUser = models.ForeignKey(User, default=None)

    def __str__(self):
        return self.subjectTitle

    def count_posts(self):
        return self.posts.count()

class Post(models.Model):
    postText = tinymce_models.HTMLField()
    postDate = models.DateField(auto_now=True)
    postSubject= models.ForeignKey(Subject, related_name='posts', blank=True, null=True)
    postUser = models.ForeignKey(User)
    postProfile = models.ForeignKey(Profile, default='versario')

    def __str__(self):
        return str(self.postUser) + ", " + str(self.postSubject)






