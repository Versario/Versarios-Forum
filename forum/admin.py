from django.contrib import admin
from forum.models import Category, Subject, Post
# Register your models here.
admin.site.register(Category)
admin.site.register(Subject)
admin.site.register(Post)

