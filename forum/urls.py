
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.forumView, name='forumListView'),
    url(r'^(?P<category_id>\d+)$', views.subjectView, name='subjectView'),
    url(r'^categories/(?P<subject_id>\d+)$', views.postView, name='postView'),
    url(r'^new_subject/(?P<category_id>\d+)$', views.newSubject, name='subjectForm'),
    url(r'^categories/new_post/(?P<subject_id>\d+)$', views.newPost, name='postForm')
]
