from forum.models import Category, Subject, Post
from accounts.models import Profile
from .forms import subjectForm, postForm
from django.shortcuts import render, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.models import User


# Forum View: Categories
def forumView(request):
    categories = Category.objects.all()
    subjects = Subject.objects.all()
    context = {'categories' : categories,
               'subjects' : subjects}
    return render(request, 'forum/forum.html', context)

# Subject View: Subjects
def subjectView(request, category_id):

    category = Category.objects.get(id=category_id)
    subjects = Subject.objects.filter(subjectCategory_id=category_id).order_by('-subjectDate')
    context = {'subjects' : subjects,
               'category' : category}
    return render(request, 'forum/subject.html', context)

# Post View: Posts
def postView(request, subject_id):

    subject = Subject.objects.get(id=subject_id)
    category = Category.objects.get(id=subject.subjectCategory_id)
    post_list = Post.objects.filter(postSubject_id=subject_id).order_by('postDate')
    paginator = Paginator(post_list, 10) # Show 10 subjects per page
    page = request.GET.get('page')

    users = []
    for post in post_list:
        users.append(post.postUser_id)

    users_in_subject = User.objects.filter(id__in=users)

    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)

    context = {'posts' : posts,
               'subject' : subject,
               'category' : category,
               'users_in_subject' : users_in_subject}

    return render(request, 'forum/post.html', context)

# Create a new subject
def newSubject(request, category_id):

    form = subjectForm(request.POST or None)
    category = Category.objects.get(id=category_id)
    user = request.user
    context = {'form': form,
               'category': category,
               'user' : user}
    if form.is_valid():
        instance = form.save(commit=False)
        instance.subjectTitle = form.cleaned_data.get('subjectTitle')
        instance.subjectCategory = category
        instance.subjectUser = user
        instance.save()
        return redirect('subjectView', category_id=category_id)

    return render(request, 'forum/new_subject.html', context)

#Create a new post
def newPost(request, subject_id):

    form = postForm(request.POST or None)
    subject = Subject.objects.get(id=subject_id)
    context = {'form' : form,
               'subject' : subject}
    if form.is_valid():
        instance = form.save(commit=False)
        user = request.user
        postText = form.cleaned_data.get('postText')
        profile = Profile.objects.get(user=user)
        instance.postText = postText
        instance.postUser = request.user
        instance.postSubject = subject
        instance.postProfile = profile
        instance.save()
        return redirect('postView', subject_id=subject_id)

    return render(request, 'forum/new_post.html', context)



