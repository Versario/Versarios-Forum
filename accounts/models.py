from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    signature = models.CharField(max_length=100, blank=True, null=True, default='No hay firma')
    description = models.TextField(max_length=500, blank=True, null=True, default='No hay descripcion')
    picture = models.ImageField(null=True, blank=True, height_field="height_field", width_field="width_field",
                                upload_to="accounts/media/")
    height_field = models.IntegerField(default=50)
    width_field = models.IntegerField(default=50)

    def __str__(self):
        return str(self.user)