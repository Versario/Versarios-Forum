from django.shortcuts import render, redirect
from django.views.generic import View
from accounts.models import Profile
from .forms import UserForm, ProfileForm
from django.contrib.auth import authenticate, login

# Cleaned (normalized) data
class UserFormView(View):
    form_class = UserForm
    template_name = 'accounts/registration_form.html'

    #display blank form
    def get(self, request):
        form = self.form_class(None)
        return render(request, self.template_name, {'form': form})

    #process form data
    def post(self, request):
        form = self.form_class(request.POST)

        if form.is_valid():
            # create obj with the form info but not send it to the database
            user = form.save(commit=False)

            #cleaned (normalized) data
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user.set_password(password)
            user.save() #save it to the database

            # returns User objects if credentials are correct

            user = authenticate(username=username, password=password)

            if user is not None:

                if user.is_active:

                    login(request, user)
                    return redirect('createProfile', user_id=user.id)

        return render(request, self.template_name, {'form': form})

def profileView(request, user_id):

    try:
        profile = Profile.objects.get(user_id=user_id)
    except Profile.DoesNotExist:
        profile = None
    context = {'profile' : profile}
    return render(request, 'accounts/profile.html', context)

def createProfile(request, user_id):

    #PENDIENTE: validar que el usuario actual sea el mismo que el que entra por parametro (user_id)
    #aunque igual submitea al usuario actual xD
    form = ProfileForm(request.POST or None, request.FILES or None)
    user = request.user
    context = {'form' : form}

    if form.is_valid():
        instance = form.save(commit=False)
        instance.user = user
        instance.save()
        return redirect('profileView', user_id=user.id)

    return render(request, 'accounts/new_profile.html', context)



