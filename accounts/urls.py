
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^profile/(?P<user_id>\d+)$', views.profileView, name='profileView'),
    url(r'^profile/create_profile/(?P<user_id>\d+)$', views.createProfile, name='createProfile')
]


